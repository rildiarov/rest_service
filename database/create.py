import sqlite3
import spech

def create_table():
    conn = sqlite3.connect(spech.database_way)
    c = conn.cursor()

    c.execute(f'''CREATE TABLE IF NOT EXISTS {spech.database_table}
                     (id INTEGER PRIMARY KEY AUTOINCREMENT,
                     login TEXT,
                     password TEXT,
                     salary REAL,
                     next_promotion_date TEXT)''')
    conn.commit()
    conn.close()
