import sqlite3
import spech

def exist_user(login):
    conn = sqlite3.connect(spech.database_way)
    c = conn.cursor()
    c.execute(f'SELECT id FROM {spech.database_table} WHERE login=?', (login, ))
    index = c.fetchone()
    conn.close()

    return bool(index)


def get_salary(login):
    conn = sqlite3.connect(spech.database_way)
    c = conn.cursor()
    c.execute(f'SELECT salary FROM {spech.database_table} WHERE login=?', (login, ))
    salary = c.fetchone()
    conn.close()

    return salary[0]


def get_next_promotion(login):
    conn = sqlite3.connect(spech.database_way)
    c = conn.cursor()
    c.execute(f'SELECT next_promotion_date FROM {spech.database_table} WHERE login=?', (login, ))
    next_promotion = c.fetchone()
    conn.close()

    return next_promotion[0]


def correct_password(login, password):
    conn = sqlite3.connect(spech.database_way)
    c = conn.cursor()
    c.execute(f'SELECT password FROM {spech.database_table} WHERE login=?', (login, ))
    password_db = c.fetchone()
    conn.close()

    return password == password_db[0]

