import hashlib
import sqlite3
from models import User
import spech

def add_new_user(user: User):
    conn = sqlite3.connect(spech.database_way)
    c = conn.cursor()
    c.execute(f"INSERT INTO {spech.database_table} (login, password, salary, next_promotion_date) VALUES (?, ?, ?, ?)",
              (user.login, user.password, user.salary, user.next_promotion))
    conn.commit()
    conn.close()


def generate_data():
    password = hashlib.sha256("12345".encode()).hexdigest()
    user = User("test_name", password, 68_500, "11.10.2023")

    add_new_user(user)
