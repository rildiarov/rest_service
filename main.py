from fastapi import FastAPI, Depends, HTTPException, status
import secrets
from parameters import token_parameters, login_parameters
from storage_tokens import Storage
import database

app = FastAPI()
tokens = Storage()
database.create_table()
database.generate_data()


@app.get("/salary/")
async def get_salary(token=Depends(token_parameters)):
    """ получение зарплаты по токену"""
    login = tokens.find_by_token(token['token'])
    if login is None:
        raise HTTPException(
            status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION,
            detail="Try regenerate token or register to system"
        )
    salary = database.get_salary("test_name")
    return {"salary": salary}


@app.get("/next_promotion/")
async def get_promotion(token=Depends(token_parameters)):
    """ Получение даты следующего повышения по токену """
    login = tokens.find_by_token(token['token'])
    if login is None:
        raise HTTPException(
            status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION,
            detail="Try regenerate token or register to system"
        )
    date = database.get_next_promotion(login)
    return {"promotion_date": date}


def generate_token():
    key = secrets.token_urlsafe(30)
    return key


@app.get("/token/")
async def get_token(login_data=Depends(login_parameters)):
    """ генерация токена для юзера """
    if not database.exist_user(login_data['login']):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверные логин",
            headers={"WWW-Authenticate": "Basic"},
        )

    if not database.correct_password(login_data['login'], login_data['password']):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверные пароль",
            headers={"WWW-Authenticate": "Password"},
        )

    token = generate_token()
    tokens.add(login_data['login'], token)

    return {"token": token}


